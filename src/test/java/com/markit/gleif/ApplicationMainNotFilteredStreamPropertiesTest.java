package com.markit.gleif;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by jc on 12/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = { "filtered.xml.stream.reader: false" })
public class ApplicationMainNotFilteredStreamPropertiesTest {

    @Test
    public void context() {
        //validates the spring configuration when 'filtered.xml.stream.reader' is false
    }
}
