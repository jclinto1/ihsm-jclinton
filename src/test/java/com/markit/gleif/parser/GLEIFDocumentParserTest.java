package com.markit.gleif.parser;

import com.google.common.collect.Lists;
import com.markit.gleif.parser.reader.CreateFilteredXMLStreamReader;
import com.markit.gleif.parser.reader.CreateXMLStreamReader;
import com.markit.gleif.parser.reader.ExcludeQNameStreamReader;
import com.markit.gleif.parser.reader.GLEIFXMLStreamReader;
import com.markit.gleif.processor.LEIRecordProcessor;
import com.markit.gleif.util.FileUtils;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.xml.namespace.QName;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jc on 10/02/2017.
 */
@RunWith(DataProviderRunner.class)
public class GLEIFDocumentParserTest {

    // Exclude theses paths when reading the XML file
    private static QName[] exclude = new QName[] {
            new QName(GLEIFDocTagNames.LEI_HEADER),
            new QName(GLEIFDocTagNames.HEAD_QUARTERS_ADDRESS),
            new QName(GLEIFDocTagNames.HEAD_QUARTERS_ADDRESS),
            new QName(GLEIFDocTagNames.REGISTRATION)
    };

    @Mock
    private LEIRecordProcessor leiRecordProcessor;

    public GLEIFDocumentParserTest() {
        MockitoAnnotations.initMocks(this);
    }

    @DataProvider
    public static Object[][] validSampleFiles() {
        return new Object[][]{
                { "20170210-GLEIF-concatenated_small_1.xml" , singleRecord()},
                { "20170210-GLEIF-concatenated_small_3.xml" , threeRecords()}
        };
    }

    @Test
    @UseDataProvider("validSampleFiles")
    public void canParseThreeRecord(String filename, List<Map<String, String>> expected) throws Exception {

        org.springframework.util.Assert.notEmpty(expected);
        File file = FileUtils.loadFile(this.getClass(), filename);

        GLEIFDocumentParser parser = new StAXGLEIFDocumentParser(new CreateFilteredXMLStreamReader(new ExcludeQNameStreamReader(exclude)), this.leiRecordProcessor);
        parser.parse(file);

        int maxTimes = 0;
        for (Map<String, String> output : expected) {
            maxTimes++;
            Mockito.verify(this.leiRecordProcessor).process(output);
        }

        Assert.assertEquals(expected.size(), maxTimes);
    }

    @DataProvider
    public static Object[][] validLargeFiles() {
        return new Object[][]{
                { "20170210-GLEIF-concatenated.xml" , 489340},
        };
    }

    @Test(timeout=30000)
    @UseDataProvider("validLargeFiles")
    public void validateCountUsingStdReader(String filename, int count) throws Exception {

        runTimedCountTest(filename, count, new CreateXMLStreamReader());
    }

    @Test(timeout=30000)
    @UseDataProvider("validLargeFiles")
    public void validateCountUsingFilteredReader(String filename, int count) throws Exception {

        runTimedCountTest(filename, count, new CreateFilteredXMLStreamReader(new ExcludeQNameStreamReader(exclude)));
    }

    private void runTimedCountTest(String filename, int count, GLEIFXMLStreamReader reader) throws Exception {
        File file = FileUtils.loadFile(this.getClass(), filename);

        GLEIFDocumentParser parser = new StAXGLEIFDocumentParser(reader, this.leiRecordProcessor);
        parser.parse(file);

        Mockito.verify(this.leiRecordProcessor, Mockito.times(count)).process(Mockito.any(Map.class));
    }

//   TODO handle missing elements in the file

//    @DataProvider
//    public static Object[][] invalidSampleFiles() {
//        return new Object[][]{
//                {"20170210-GLEIF-concatenated_small_1_invalid_missing_legal_name.xml", empty()}
//        };
//    }
//
//    @Test
//    @UseDataProvider("invalidSampleFiles")
//    public void canHandleMissingElements(String filename, List<Map<String, String>> expected) throws Exception {
//
//        File file = FileUtils.loadFile(this.getClass(), filename);
//
//        GLEIFDocumentParser parser = new StAXGLEIFDocumentParser(new CreateFilteredXMLStreamReader(new ExcludeQNameStreamReader(exclude)), this.leiRecordProcessor);
//        parser.parse(file);
//
//        int maxTimes = 0;
//        for (Map<String, String> output : expected) {
//            maxTimes++;
//            Mockito.verify(this.leiRecordProcessor).process(output);
//        }
//
//        if(expected.size() == 0) {
//            Mockito.verifyZeroInteractions(this.leiRecordProcessor);
//        }
//
//        Assert.assertEquals(expected.size(), maxTimes);
//    }

    private static Object empty() {
        return Lists.newArrayList();
    }

    private static Object singleRecord() {
        return Lists.newArrayList(record1());
    }

    private static Object threeRecords() {
        return Lists.newArrayList(record1(), record2(), record3());
    }

    private static Map<String, String> record1() {
        Map<String, String> recordMap1 = new HashMap<>();
        recordMap1.put("LEI", "4469000001AVO26P9X86");
        recordMap1.put("LegalName", "ASOCIACION MEXICANA DE ESTANDARES PARA EL COMERCIO ELECTRONICO AC");
        recordMap1.put("Country", "MX");
        recordMap1.put("EntityStatus", "ACTIVE");
        return recordMap1;
    }

    private static Map<String, String> record3() {
        Map<String, String> recordMap3 = new HashMap<>();
        recordMap3.put("LEI", "485100004VOFFO18DD84");
        recordMap3.put("LegalName", "NLB VITA, življenjska zavarovalnica d.d. Ljubljana");
        recordMap3.put("Country", "SI");
        recordMap3.put("EntityStatus", "ACTIVE");
        return recordMap3;
    }

    private static Map<String, String> record2() {
        Map<String, String> recordMap2 = new HashMap<>();
        recordMap2.put("LEI", "48510000JZ17NWGUA510");
        recordMap2.put("LegalName", "KDD - Centralna klirinško depotna družba delniška družba");
        recordMap2.put("Country", "SI");
        recordMap2.put("EntityStatus", "ACTIVE");
        return recordMap2;
    }

    @Before
    public void resetMocks() {
        Mockito.reset(this.leiRecordProcessor);
    }
}
