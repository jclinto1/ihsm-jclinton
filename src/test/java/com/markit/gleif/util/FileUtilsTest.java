package com.markit.gleif.util;

import org.junit.Test;

import java.io.FileNotFoundException;

public class FileUtilsTest {

    @Test
    public void loadsFileOK() throws Exception {
        FileUtils.loadFile(FileUtilsTest.class, "file-utils.dat");
    }

    @Test(expected = FileNotFoundException.class)
    public void loadsFileMissing() throws Exception {
        FileUtils.loadFile(FileUtilsTest.class, "file-utils-missing.tmp");
    }
}
