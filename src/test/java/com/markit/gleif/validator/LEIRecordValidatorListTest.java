package com.markit.gleif.validator;

import com.google.common.collect.Lists;
import com.markit.gleif.domain.LEIRecordTestHelper;
import com.markit.gleif.validator.field.country.CountryMaxFieldLengthValidator;
import com.markit.gleif.validator.field.country.CountryFieldMandatorValidator;
import com.markit.gleif.validator.field.lei.LEIMaxFieldLengthValidator;
import com.markit.gleif.validator.field.lei.LEIFieldMandatorValidator;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Map;

/**
 * Created by jc on 12/02/2017.
 */
@RunWith(DataProviderRunner.class)
public class LEIRecordValidatorListTest {

    @DataProvider
    public static Object[][] passesValidation() {
        return new Object[][]{
                { Lists.newArrayList()},
                { Lists.newArrayList(new LEIMaxFieldLengthValidator(100))},
                { Lists.newArrayList(new CountryMaxFieldLengthValidator(100))},
                { Lists.newArrayList(new LEIMaxFieldLengthValidator(100), new CountryMaxFieldLengthValidator(100))},
                { Lists.newArrayList(new LEIFieldMandatorValidator(true))},
                { Lists.newArrayList(new CountryFieldMandatorValidator(true))},
                { Lists.newArrayList(new LEIFieldMandatorValidator(true), new CountryFieldMandatorValidator(true))},
        };
    }

    @Test
    @UseDataProvider("passesValidation")
    public void passesValidation(List<LEIRecordValidator> validators) {
        Map<String, String> record = LEIRecordTestHelper.validRecord();
        LEIRecordValidator list = new LEIRecordValidatorList(validators);
        boolean valid = list.isValid(record);
        Assert.assertTrue(valid);
    }

    @DataProvider
    public static Object[][] failsValidation() {
        return new Object[][]{
                { Lists.newArrayList(new LEIMaxFieldLengthValidator(1)) },
                { Lists.newArrayList(new CountryMaxFieldLengthValidator(1)) },
                { Lists.newArrayList(new LEIMaxFieldLengthValidator(1), new CountryMaxFieldLengthValidator(1))},
        };
    }

    @Test
    @UseDataProvider("failsValidation")
    public void failsValidation(List<LEIRecordValidator> validators) {
        Map<String, String> record = LEIRecordTestHelper.validRecord();
        LEIRecordValidator list = new LEIRecordValidatorList(validators);
        boolean valid = list.isValid(record);
        Assert.assertFalse(valid);
    }

    @DataProvider
    public static Object[][] failsMandatoryValidation() {
        return new Object[][]{
                { Lists.newArrayList(new LEIFieldMandatorValidator(true))},
                { Lists.newArrayList(new CountryFieldMandatorValidator(true))},
                { Lists.newArrayList(new LEIFieldMandatorValidator(true), new CountryFieldMandatorValidator(true))},
        };
    }

    @Test
    @UseDataProvider("failsMandatoryValidation")
    public void failsMandatoryValidation(List<LEIRecordValidator> validators) {
        Map<String, String> record = LEIRecordTestHelper.nullValues();
        LEIRecordValidator list = new LEIRecordValidatorList(validators);
        boolean valid = list.isValid(record);
        Assert.assertFalse(valid);
    }
}
