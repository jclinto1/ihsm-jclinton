package com.markit.gleif.dao;

import com.markit.gleif.dao.jpa.LEIRecordEntity;
import com.markit.gleif.dao.jpa.LEIRecordRepository;
import com.markit.gleif.domain.LEIRecordTestHelper;
import com.markit.gleif.parser.GLEIFDocTagNames;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * Created by jc on 12/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JpaLEIRecordDaoTest {

    @Autowired
    private JpaLEIRecordDao jpaLEIRecordDao;

    @Autowired
    private LEIRecordRepository leiRecordRepository;

    @Test
    public void canPersist() {
        Map<String, String> validRecord = LEIRecordTestHelper.validRecord();
        this.jpaLEIRecordDao.persit(validRecord);

        LEIRecordEntity persisted = this.leiRecordRepository.findOne(validRecord.get(GLEIFDocTagNames.LEI));
        Assert.assertNotNull(persisted);
    }
}
