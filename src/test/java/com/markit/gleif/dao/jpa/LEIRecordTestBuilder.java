package com.markit.gleif.dao.jpa;

/**
 * Created by jc on 12/02/2017.
 */
public class LEIRecordTestBuilder {

    private String lei = "someLei";
    private String legalName = "someLegalName";
    private String country = "someCountry";
    private String status = "someStatus";

    public LEIRecordEntity build() {
        return new LEIRecordEntity(this.lei, this.legalName, this.country, this.status);
    }

    public LEIRecordTestBuilder lei(String value) {
        this.lei = value;
        return this;
    }

    public LEIRecordTestBuilder legalName(String value) {
        this.legalName = value;
        return this;
    }

    public LEIRecordTestBuilder country(String value) {
        this.country = value;
        return this;
    }

    public LEIRecordTestBuilder status(String value) {
        this.status = value;
        return this;
    }
}
