package com.markit.gleif.dao.jpa;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by jc on 12/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LEIRecordRepositoryTest {

    @Autowired
    private LEIRecordRepository leiRecordRepository;

    @Test
    public void canPersist() {
        LEIRecordEntity record = new LEIRecordTestBuilder().build();
        this.leiRecordRepository.save(record);
        LEIRecordEntity persisted = this.leiRecordRepository.findOne(record.getId());
        Assert.assertNotNull(persisted);
    }


}
