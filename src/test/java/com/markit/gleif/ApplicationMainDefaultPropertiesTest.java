package com.markit.gleif;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by jc on 12/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationMainDefaultPropertiesTest {

    @Test
    public void context() {
        //validates the spring configuration
    }
}
