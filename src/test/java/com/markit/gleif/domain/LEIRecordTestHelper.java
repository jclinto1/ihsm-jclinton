package com.markit.gleif.domain;

import com.markit.gleif.parser.GLEIFDocTagNames;

import java.util.HashMap;
import java.util.Map;

public final class LEIRecordTestHelper {

    public static Map<String, String> validRecord() {
        Map<String, String> record = new HashMap<>(1);
        record.put(GLEIFDocTagNames.LEI, "someLEI");
        record.put(GLEIFDocTagNames.LEGAL_NAME, "someLegalName");
        record.put(GLEIFDocTagNames.COUNTRY, "US");
        record.put(GLEIFDocTagNames.ENTITY_STATUS, "ACTIVE");
        return record;
    }

    public static Map<String, String> nullValues() {
        Map<String, String> record = new HashMap<>(1);
        record.put(GLEIFDocTagNames.LEI, null);
        record.put(GLEIFDocTagNames.LEGAL_NAME, null);
        record.put(GLEIFDocTagNames.COUNTRY, null);
        record.put(GLEIFDocTagNames.ENTITY_STATUS, null);
        return record;
    }
}
