package com.markit.gleif.dao;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.markit.gleif.dao.jpa.LEIRecordEntity;
import com.markit.gleif.dao.jpa.LEIRecordRepository;
import com.markit.gleif.parser.GLEIFDocTagNames;

import java.io.File;
import java.util.Map;

/**
 * JPA implementation uses Spring Data to build the CRUD interface.
 *
 * Created by jc on 10/02/2017.
 */
public class JpaLEIRecordDao implements LEIRecordDao {

    private LEIRecordRepository leiRecordRepository;

    public JpaLEIRecordDao(LEIRecordRepository leiRecordRepository) {
        this.leiRecordRepository = leiRecordRepository;
    }

    @Override
    public void persit(Map<String, String> record) {

        LEIRecordEntity entity = new LEIRecordEntity(
                record.get(GLEIFDocTagNames.LEI),
                record.get(GLEIFDocTagNames.LEGAL_NAME),
                record.get(GLEIFDocTagNames.COUNTRY),
                record.get(GLEIFDocTagNames.ENTITY_STATUS)
        );

        this.leiRecordRepository.save(entity);

        dumpTimes(entity);
    }

    private void dumpTimes(LEIRecordEntity entity) {
        // '378900E0A78B7549C212' is the last record in the file

        if("378900E0A78B7549C212".equals(entity.getId())) {

            try {

                long end = System.currentTimeMillis();

                String startTime = "\nend time: " + end + "\n";
                System.out.println(startTime);

                Files.append(startTime, new File("timings.txt"), Charsets.UTF_8);

                System.out.println("Ctrl-C to exit");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
