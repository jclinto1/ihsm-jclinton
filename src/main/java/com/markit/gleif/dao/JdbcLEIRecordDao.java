package com.markit.gleif.dao;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Map;

/**
 * Created by jc on 10/02/2017.
 */
public class JdbcLEIRecordDao implements LEIRecordDao {

    private JdbcTemplate jdbcTemplate;

    public JdbcLEIRecordDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void persit(Map<String, String> record) {

        /**
         * Non implemented example showing how we could use simple JDBC template from
         * Spring if desired
         *
         * this.jdbcTemplate.execute(...)
         */

        throw new UnsupportedOperationException("Currently not supported");
    }
}
