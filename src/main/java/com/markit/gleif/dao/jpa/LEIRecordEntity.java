package com.markit.gleif.dao.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "legal_entities")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LEIRecordEntity {

    @Id
    private String id;

    @Column(length = 1000)
    private String legalName;

    @Column()
    private String country;

    @Column()
    private String status;

    public LEIRecordEntity() {
    }

    public LEIRecordEntity(String id, String legalName, String country, String status) {
        this.id = id;
        this.legalName = legalName;
        this.country = country;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LEIRecordEntity{");
        sb.append("id='").append(id).append('\'');
        sb.append(", legalName='").append(legalName).append('\'');
        sb.append(", country='").append(country).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

