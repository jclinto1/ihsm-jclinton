package com.markit.gleif.dao.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Spring dynamically provides the implementation unless overridden
 */
public interface LEIRecordRepository extends PagingAndSortingRepository<LEIRecordEntity, String> {

}
