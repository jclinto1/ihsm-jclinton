package com.markit.gleif.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by jc on 12/02/2017.
 */
public final class FileUtils {

    public static File loadFile(Class cls, String filename) throws URISyntaxException, FileNotFoundException {
        URL resource = cls.getClassLoader().getResource(filename);
        if(resource == null) {
            throw new FileNotFoundException("filename not found given cls '" + cls + "' and name '" + filename + "'");
        }
        return new File(resource.toURI());
    }
}
