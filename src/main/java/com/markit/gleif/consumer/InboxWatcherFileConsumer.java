package com.markit.gleif.consumer;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.markit.gleif.parser.GLEIFDocumentParser;

import java.io.File;

/**
 * This class "is going to be designed" for listening for new files an playing them through, but
 * for the interview demo the file on the classpath is loaded in.
 *
 * Created by jc on 10/02/2017.
 */
public class InboxWatcherFileConsumer implements FileConsumer {

    private final String filename;
    private final GLEIFDocumentParser gleifDocumentParser;

    public InboxWatcherFileConsumer(String filename, GLEIFDocumentParser gleifDocumentParser) {
        this.filename = filename;
        this.gleifDocumentParser = gleifDocumentParser;
    }

    public void consumer() {

        try {

            System.out.println(filename);

            File file = new File(this.filename);

            long start = System.currentTimeMillis();

            String startTime = "start time: " + start;
            System.out.println(startTime);

            Files.write(startTime, new File("timings.txt"), Charsets.UTF_8);

            this.gleifDocumentParser.parse(file);

        } catch (Exception e) {
            e.printStackTrace();
        }

        // TODO implement File Watcher API for files that get dropped in to an INBOX dir
    }
}
