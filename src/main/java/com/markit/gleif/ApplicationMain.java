package com.markit.gleif;

/**
 * Created by jc on 10/02/2017.
 */
import com.markit.gleif.consumer.InboxWatcherFileConsumer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.markit.gleif.dao.jpa")
public class ApplicationMain {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(ApplicationMain.class, args);
        run.getBean(InboxWatcherFileConsumer.class).consumer();

    }
}
