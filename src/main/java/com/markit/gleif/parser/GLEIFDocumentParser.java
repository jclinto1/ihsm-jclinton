package com.markit.gleif.parser;

import java.io.File;
import java.util.Optional;

/**
 * Created by jc on 10/02/2017.
 */
public interface GLEIFDocumentParser<T> {

    Optional<T> parse(File file);
}
