package com.markit.gleif.parser;

import com.markit.gleif.parser.reader.GLEIFXMLStreamReader;
import com.markit.gleif.processor.LEIRecordProcessor;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Uses StAX to read the XML document and when complete parses that
 * to the {@link #leiRecordProcessor} for further processing.
 *
 * Created by jc on 10/02/2017.
 */
public class StAXGLEIFDocumentParser implements GLEIFDocumentParser<Void> {

    private final GLEIFXMLStreamReader streamReader;
    private final LEIRecordProcessor leiRecordProcessor;

    public StAXGLEIFDocumentParser(GLEIFXMLStreamReader streamReader, LEIRecordProcessor leiRecordProcessor) {
        this.streamReader = streamReader;
        this.leiRecordProcessor = leiRecordProcessor;
    }

    @Override
    public Optional<Void> parse(File xmlDoc) {

        try {

            XMLStreamReader reader = this.streamReader.fromFile(xmlDoc);

            readStream(reader);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    private void readStream(XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String localName = reader.getLocalName();
                    if (localName.equals(GLEIFDocTagNames.LEI_RECORDS))
                        readLEIRecords(reader);
                    break;
            }
        }
    }

    private void readLEIRecords(XMLStreamReader reader) throws XMLStreamException {

        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String localName = reader.getLocalName();
                    if (localName.equals(GLEIFDocTagNames.LEI_RECORD)) {
                        readLEIRecordItem(reader);
                    }
                    break;

            }
        }
    }

    private void readLEIRecordItem(XMLStreamReader reader) throws XMLStreamException {
        Map<String,String> record = new HashMap<>();

        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String localName = reader.getLocalName();
                    if (localName.equals(GLEIFDocTagNames.LEI)) {
                        String value = text(reader);
                        record.put(GLEIFDocTagNames.LEI, value);
                    } else if (localName.equals(GLEIFDocTagNames.ENTITY)) {
                        readEntity(reader, record);
                        this.leiRecordProcessor.process(record);
                    }
                    break;
                case XMLStreamReader.END_ELEMENT:
                    // Return control to the parent function to manage the scanning
                    if (isLeiRecordTag(reader)) {
                        return;
                    }
                    break;
            }
        }
    }

    private void readEntity(XMLStreamReader reader, Map<String,String> record) throws XMLStreamException {

        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String localName = reader.getLocalName();
                    if (localName.equals(GLEIFDocTagNames.LEGAL_NAME)) {
                        String value = text(reader);
                        record.put(GLEIFDocTagNames.LEGAL_NAME, value);
                    } else if (localName.equals(GLEIFDocTagNames.LEGAL_ADDRESS)) {
                        readLegalAddress(reader, record);
                    } else if (localName.equals(GLEIFDocTagNames.ENTITY_STATUS)) {
                        String value = text(reader);
                        record.put(GLEIFDocTagNames.ENTITY_STATUS, value);
                    }
                    break;
                case XMLStreamReader.END_ELEMENT:
                    // Return control to the parent function to manage the scanning
                    if (isEntityTag(reader)) {
                        return;
                    }
                    break;
            }
        }
    }


    private void readLegalAddress(XMLStreamReader reader, Map<String,String> record) throws XMLStreamException {

        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String localName = reader.getLocalName();
                    if (localName.equals(GLEIFDocTagNames.COUNTRY)) {
                        String value = text(reader);
                        record.putIfAbsent(GLEIFDocTagNames.COUNTRY, value);
                    }
                    break;
                case XMLStreamReader.END_ELEMENT:
                    // Return control to the parent function to manage the scanning
                    if (reader.getLocalName().equals(GLEIFDocTagNames.LEGAL_ADDRESS)) {
                        return;
                    }
                    break;
            }
        }
    }

    private String text(XMLStreamReader reader) throws XMLStreamException {
        StringBuilder buffer = new StringBuilder();
        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.CHARACTERS:
                    String text = reader.getText();
                    buffer.append(text);
                    break;
                case XMLStreamReader.END_ELEMENT:
                    String result = buffer.toString();
                    return result;
            }
        }
        throw new XMLStreamException("Error reading text");
    }


    private boolean isLeiRecordTag(XMLStreamReader reader) {
        return reader.getLocalName().equals(GLEIFDocTagNames.LEI_RECORD);
    }

    private boolean isEntityTag(XMLStreamReader reader) {
        return reader.getLocalName().equals(GLEIFDocTagNames.ENTITY);
    }
}
