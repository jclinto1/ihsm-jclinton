package com.markit.gleif.parser.reader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileReader;

/**
 * Creates an XML Stream Reader that processing all StAX events.
 *
 * Created by jc on 12/02/2017.
 */
public class CreateXMLStreamReader implements GLEIFXMLStreamReader {

    @Override
    public XMLStreamReader fromFile(File xmlDoc) throws Exception {

        XMLInputFactory factory = XMLInputFactory.newInstance();

        FileReader filerReader = new FileReader(xmlDoc);

        XMLStreamReader reader = factory.createXMLStreamReader(filerReader);

        return reader;
    }
}
