package com.markit.gleif.parser.reader;

import javax.xml.namespace.QName;
import javax.xml.stream.Location;
import javax.xml.stream.StreamFilter;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

/**
 * Excludes tags from the scanning process.
 *
 * For information
 * @see https://www.ibm.com/developerworks/library/x-tipstx2/index.html
 *
 * Created by jc on 12/02/2017.
 */
public class ExcludeQNameStreamReader implements StreamFilter {

    private final QName[] exclude;

    public ExcludeQNameStreamReader(QName[] exclude) {
        this.exclude = exclude;
    }

    // Element level
    int depth = -1;
    // Last matching path segment
    int match = -1;
    // Filter result
    boolean process = true;
    // Character position in document
    int currentPos = -1;

    @Override
    public boolean accept(XMLStreamReader reader) {

        // Get character position
        Location loc = reader.getLocation();
        int pos = loc.getCharacterOffset();
        // Inhibit double execution
        if (pos != currentPos) {
            currentPos = pos;
            switch (reader.getEventType()) {
                case XMLStreamConstants.START_ELEMENT :
                    // Increment element depth
                    if (++depth < exclude.length && match == depth - 1) {
                        // Compare path segment with current element
                        QName qName = reader.getName();
                        QName objectToTest = exclude[depth];
                        if (qName.getLocalPart().equals(objectToTest.getLocalPart()))
                            // Equal - set segment pointer
                            match = depth;
                    }
                    // Process all elements not in path
                    process = match < exclude.length - 1;
                    break;
                // End of XML element
                case XMLStreamConstants.END_ELEMENT :
                    // Process all elements not in path
                    process = match < exclude.length - 1;
                    // Decrement element depth
                    if (--depth < match)
                        // Update segment pointer
                        match = depth;
                    break;
            }
        }
        return process;
    }
}
