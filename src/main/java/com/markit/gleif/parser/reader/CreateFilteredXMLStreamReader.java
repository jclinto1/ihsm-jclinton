package com.markit.gleif.parser.reader;

import javax.xml.stream.*;
import java.io.File;
import java.io.FileReader;

/**
 * Creates an XML Stream Reader that processing all StAX events.
 *
 * For information on the stream filter see IBM site
 * @see https://www.ibm.com/developerworks/library/x-tipstx2/index.html
 *
 * Created by jc on 12/02/2017.
 */
public final class CreateFilteredXMLStreamReader implements GLEIFXMLStreamReader {

    private final StreamFilter streamFilter;

    public CreateFilteredXMLStreamReader(StreamFilter streamFilter) {
        this.streamFilter = streamFilter;
    }

    @Override
    public XMLStreamReader fromFile(File xmlDoc) throws Exception {

        XMLInputFactory factory = XMLInputFactory.newInstance();

        XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(xmlDoc));

        XMLStreamReader filteredReader = factory.createFilteredReader(reader, this.streamFilter);

        return filteredReader;
    }
}
