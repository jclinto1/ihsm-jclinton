package com.markit.gleif.parser.reader;

import javax.xml.stream.XMLStreamReader;
import java.io.File;

/**
 * Created by jc on 12/02/2017.
 */
public interface GLEIFXMLStreamReader {

    XMLStreamReader fromFile(File xmlDoc) throws Exception;
}
