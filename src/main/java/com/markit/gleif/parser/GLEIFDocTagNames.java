package com.markit.gleif.parser;

/**
 * Document tag names.
 *
 * Created by jc on 12/02/2017.
 */
public final class GLEIFDocTagNames {


    public static final String LEI_HEADER = "LEIHeader";
    public static final String LEI_RECORDS = "LEIRecords";
    public static final String LEI_RECORD = "LEIRecord";
    public static final String LEI = "LEI";
    public static final String ENTITY = "Entity";
    public static final String LEGAL_NAME = "LegalName";
    public static final String LEGAL_ADDRESS = "LegalAddress";
    public static final String ENTITY_STATUS = "EntityStatus";
    public static final String COUNTRY = "Country";
    public static final String HEAD_QUARTERS_ADDRESS = "HeadquartersAddress";
    public static final String BUSINESS_REGISTER_ENTITY_ID = "BusinessRegisterEntityID";
    public static final String REGISTRATION = "Registration";
}
