package com.markit.gleif.spring.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Bind the values in the application.yml file starting with the given prefix.
 *
 * <p>Created by jc on 12/02/2017.</p>
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "filtered.xml.stream")
public class ExcludedQNames {

    private boolean reader;
    private List<String> qnames;

    public boolean isReader() {
        return reader;
    }

    public void setReader(boolean reader) {
        this.reader = reader;
    }

    public List<String> getQnames() {
        return qnames;
    }

    public void setQnames(List<String> qnames) {
        this.qnames = qnames;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExcludedQNames{");
        sb.append("qnames=").append(qnames);
        sb.append('}');
        return sb.toString();
    }
}
