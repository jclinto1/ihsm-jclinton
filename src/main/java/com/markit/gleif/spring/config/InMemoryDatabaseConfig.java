package com.markit.gleif.spring.config;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Configure the in memory database
 *
 * <p>Created by jc on 10/02/2017.</p>
 */
@Configuration
public class InMemoryDatabaseConfig implements EnvironmentAware {

    private Environment environment;

//    @Bean
//    public DataSource dataSource() {
//        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
//        EmbeddedDatabase embeddedDatabase = builder
//                .setType(EmbeddedDatabaseType.H2)
//                .setName("markit")
//                .addScript("db/sql/create-tables.sql")
//                .build();
//        return embeddedDatabase;
//    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}