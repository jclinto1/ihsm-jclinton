package com.markit.gleif.spring.config;

import com.google.common.collect.Lists;
import com.markit.gleif.parser.reader.CreateFilteredXMLStreamReader;
import com.markit.gleif.parser.reader.CreateXMLStreamReader;
import com.markit.gleif.parser.reader.ExcludeQNameStreamReader;
import com.markit.gleif.parser.reader.GLEIFXMLStreamReader;
import com.markit.gleif.spring.properties.ExcludedQNames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.xml.namespace.QName;
import java.util.ArrayList;
import java.util.List;

/**
 * Configure the Stream Reader options.
 *
 * <p>Injected stream readers are determine by the property {@code filtered.xml.stream.reader} in the application.yml</p>
 *
 * <p>Created by jc on 10/02/2017.</p>
 */
@Configuration
public class StreamReaderConfig implements EnvironmentAware {

    private Environment environment;

    @Autowired
    private ExcludedQNames excludedQNames;

    @Bean
    @ConditionalOnProperty(value = "filtered.xml.stream.reader", havingValue = "false")
    public GLEIFXMLStreamReader createXMLStreamReader() {
        return new CreateXMLStreamReader();
    }

    @Bean
    @ConditionalOnProperty(value = "filtered.xml.stream.reader", havingValue = "true")
    public GLEIFXMLStreamReader createFilteredXMLStreamReader() {

        List<String> property = this.excludedQNames.getQnames();

        if(property.isEmpty()) {
            throw new RuntimeException("Possible mis-configuration, filtered stream reader enabled without any excluded QNames");
        }

        QName[] qNamesToExclude = new QName[property.size()];

        int i=0;
        for(String qnameText : property) {
            qNamesToExclude[i] = new QName(qnameText);
        }

        return new CreateFilteredXMLStreamReader(new ExcludeQNameStreamReader(qNamesToExclude));
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}