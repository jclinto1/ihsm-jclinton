package com.markit.gleif.spring.config;

import com.google.common.collect.Lists;
import com.markit.gleif.validator.field.country.CountryMaxFieldLengthValidator;
import com.markit.gleif.validator.field.lei.LEIMaxFieldLengthValidator;
import com.markit.gleif.validator.LEIRecordValidator;
import com.markit.gleif.validator.LEIRecordValidatorList;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Configure the validators in the system
 *
 * <p>Created by jc on 10/02/2017.</p>
 */
@Configuration
public class RecordValidatorConfig implements EnvironmentAware {

    private Environment environment;

    /**
     * Group validator bean of field validators
     */
    @Bean
    public LEIRecordValidator leiRecordValidator() {
        return new LEIRecordValidatorList(Lists.newArrayList(
                leiFieldLengthValidator(),
                countryFieldLengthValidator()));
    }

    private LEIRecordValidator leiFieldLengthValidator() {
        Integer size = environment.getProperty("field.lei.validate.max.size", Integer.class);
        if(size == null) {
            size = 1000;
        }
        return new LEIMaxFieldLengthValidator(size);
    }

    private LEIRecordValidator countryFieldLengthValidator() {
        Integer size = environment.getProperty("field.country.validate.max.size", Integer.class);
        if(size == null) {
            size = 1000;
        }
        return new CountryMaxFieldLengthValidator(size);
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
