package com.markit.gleif.spring.config;

import com.markit.gleif.consumer.FileConsumer;
import com.markit.gleif.consumer.InboxWatcherFileConsumer;
import com.markit.gleif.dao.JdbcLEIRecordDao;
import com.markit.gleif.dao.JpaLEIRecordDao;
import com.markit.gleif.dao.LEIRecordDao;
import com.markit.gleif.dao.jpa.LEIRecordRepository;
import com.markit.gleif.parser.GLEIFDocumentParser;
import com.markit.gleif.parser.StAXGLEIFDocumentParser;
import com.markit.gleif.parser.reader.GLEIFXMLStreamReader;
import com.markit.gleif.processor.ConcurrentLEIRecordProcessor;
import com.markit.gleif.processor.LEIRecordProcessor;
import com.markit.gleif.validator.LEIRecordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Configure the Main Spring Beans
 *
 * <p>Created by jc on 10/02/2017.</p>
 */
@Configuration
public class MainConfig implements EnvironmentAware {

    private Environment environment;

    /**
     * Either std or filtered will be injected based on application.yml config, see property filtered.xml.stream.reader
     */
    @Autowired
    private GLEIFXMLStreamReader gleifxmlStreamReader;

    @Autowired
    private LEIRecordValidator leiRecordValidator;

    @Autowired
    private LEIRecordRepository leiRecordRepository;

    @Bean
    public FileConsumer inboxWatcherFileConsumer() {
        String xmlDocFilename = environment.getRequiredProperty("gleif.filename");
        return new InboxWatcherFileConsumer(xmlDocFilename, gleifDocumentParser());
    }

    @Bean
    public LEIRecordDao leiRecordDao() {
        return new JpaLEIRecordDao(this.leiRecordRepository);
    }

    @Bean
    public LEIRecordProcessor leiRecordProcessor() {
        Integer threads = environment.getProperty("threads", Integer.class);

        if(threads == null) {
            threads = Runtime.getRuntime().availableProcessors();
        }

        return new ConcurrentLEIRecordProcessor(this.leiRecordValidator, leiRecordDao(), threads);
    }

    @Bean
    public GLEIFDocumentParser gleifDocumentParser() {
        return new StAXGLEIFDocumentParser(this.gleifxmlStreamReader, leiRecordProcessor());
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
