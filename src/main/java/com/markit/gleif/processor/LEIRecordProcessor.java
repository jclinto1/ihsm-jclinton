package com.markit.gleif.processor;

import java.util.Map;

/**
 * Created by jc on 10/02/2017.
 */
public interface LEIRecordProcessor {

    void process(Map<String,String> record);
}
