package com.markit.gleif.processor;

import com.markit.gleif.dao.LEIRecordDao;
import com.markit.gleif.validator.LEIRecordValidator;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jc on 10/02/2017.
 */
public class ConcurrentLEIRecordProcessor implements LEIRecordProcessor {

    private final LEIRecordValidator leiRecordValidator;
    private final LEIRecordDao leiRecordDao;
    private final ExecutorService executorService;

    public ConcurrentLEIRecordProcessor(LEIRecordValidator leiRecordValidator, LEIRecordDao leiRecordDao, int threads) {
        this.leiRecordValidator = leiRecordValidator;
        this.leiRecordDao = leiRecordDao;
        this.executorService = Executors.newFixedThreadPool(threads);
    }

    @Override
    public void process(final Map<String, String> record) {

        boolean valid = this.leiRecordValidator.isValid(record);

        if(valid == false) {
            handleWhenInvalid(record);
        } else {
            handleWhenValidate(record);
        }

    }

    private void handleWhenInvalid(Map<String, String> record) {

        //metric post

        //log

    }

    private void handleWhenValidate(final Map<String, String> record) {
        this.executorService.execute(new Runnable() {
            @Override
            public void run() {
                leiRecordDao.persit(record);
            }
        });
    }
}
