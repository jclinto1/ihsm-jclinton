package com.markit.gleif.validator;

import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * LEI Field Length Validator
 *
 * <p>Created by jc on 12/02/2017.</p>
 */
public abstract class MaxFieldLengthValidator implements LEIRecordValidator {

    private final int maxSize;

    /**
     * Override in sub class and return the field key under validation, constants defined
     * in class {@link com.markit.gleif.parser.GLEIFDocTagNames}
     *
     * @return String field key
     */
    protected abstract String recordFieldKey();

    public MaxFieldLengthValidator(int maxSize) {
        this.maxSize = maxSize;

        if(this.maxSize < 0) {
            throw new IllegalArgumentException("MaxFieldLengthValidator 'maxSize' less than 0");
        }
    }

    @Override
    public final boolean isValid(Map<String, String> record) {

        String fieldKey = recordFieldKey();

        if(StringUtils.hasText(fieldKey) == false) {
            throw new IllegalArgumentException("Provided 'fieldKey' should never be null");
        }

        String text = record.get(fieldKey);

        if(StringUtils.hasText(text) == false) {
            return false;
        }

        if(text.length() > maxSize) {
            return false;
        }

        return true;
    }

}
