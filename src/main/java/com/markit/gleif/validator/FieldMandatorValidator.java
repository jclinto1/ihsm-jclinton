package com.markit.gleif.validator;

import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * LEI Field Mandatory Field Validator
 *
 * Created by jc on 12/02/2017.
 */
public abstract class FieldMandatorValidator implements LEIRecordValidator {

    private final boolean mandatory;

    public FieldMandatorValidator(boolean mandatory) {
        this.mandatory = mandatory;
    }

    /**
     * Override in sub class and return the field key under validation, constants defined
     * in class {@link com.markit.gleif.parser.GLEIFDocTagNames}
     *
     * @return String field key
     */
    protected abstract String recordFieldKey();


    @Override
    public final boolean isValid(Map<String, String> record) {

        if(this.mandatory == false) {
            return true;
        }

        String fieldKey = recordFieldKey();

        if(StringUtils.hasText(fieldKey) == false) {
            throw new IllegalArgumentException("Provided 'fieldKey' should never be null");
        }

        String text = record.get(fieldKey);

        if(StringUtils.hasText(text) == false) {
            return false;
        }

        return true;
    }

}
