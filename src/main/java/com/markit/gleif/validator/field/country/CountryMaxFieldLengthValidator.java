package com.markit.gleif.validator.field.country;

import com.markit.gleif.parser.GLEIFDocTagNames;
import com.markit.gleif.validator.MaxFieldLengthValidator;

/**
 * Country Field Length Validator
 *
 * <p>Created by jc on 12/02/2017.</p>
 *
 */
public class CountryMaxFieldLengthValidator extends MaxFieldLengthValidator {

    public CountryMaxFieldLengthValidator(int minSize) {
        super(minSize);
    }

    @Override
    protected String recordFieldKey() {
        return GLEIFDocTagNames.COUNTRY;
    }
}
