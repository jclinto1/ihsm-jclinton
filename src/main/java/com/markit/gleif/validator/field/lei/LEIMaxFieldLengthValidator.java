package com.markit.gleif.validator.field.lei;

import com.markit.gleif.parser.GLEIFDocTagNames;
import com.markit.gleif.validator.MaxFieldLengthValidator;

/**
 * LEI Field Length Validator
 *
 * Created by jc on 12/02/2017.
 */
public class LEIMaxFieldLengthValidator extends MaxFieldLengthValidator {

    public LEIMaxFieldLengthValidator(int minSize) {
        super(minSize);
    }

    @Override
    protected String recordFieldKey() {
        return GLEIFDocTagNames.LEI;
    }
}
