package com.markit.gleif.validator.field.country;

import com.markit.gleif.parser.GLEIFDocTagNames;
import com.markit.gleif.validator.FieldMandatorValidator;

/**
 * Country Field Mandatory Validator
 *
 * <p>Created by jc on 12/02/2017.</p>
 *
 */
public class CountryFieldMandatorValidator extends FieldMandatorValidator {

    public CountryFieldMandatorValidator(boolean mandatory) {
        super(mandatory);
    }

    @Override
    protected String recordFieldKey() {
        return GLEIFDocTagNames.COUNTRY;
    }
}
