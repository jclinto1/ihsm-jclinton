package com.markit.gleif.validator.field.lei;

import com.markit.gleif.parser.GLEIFDocTagNames;
import com.markit.gleif.validator.FieldMandatorValidator;

/**
 * LEI Field Mandatory Validator
 *
 * Created by jc on 12/02/2017.
 */
public class LEIFieldMandatorValidator extends FieldMandatorValidator {

    public LEIFieldMandatorValidator(boolean mandatory) {
        super(mandatory);
    }

    @Override
    protected String recordFieldKey() {
        return GLEIFDocTagNames.LEI;
    }
}
