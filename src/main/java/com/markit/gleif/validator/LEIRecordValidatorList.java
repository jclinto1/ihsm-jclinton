package com.markit.gleif.validator;

import java.util.List;
import java.util.Map;

/**
 * Created by jc on 12/02/2017.
 */
public class LEIRecordValidatorList implements LEIRecordValidator {

    private List<LEIRecordValidator> validatorList;

    public LEIRecordValidatorList(List<LEIRecordValidator> validatorList) {
        this.validatorList = validatorList;
    }

    @Override
    public boolean isValid(Map<String, String> record) {

        if(this.validatorList.isEmpty()) {
            return true;
        }

        for(LEIRecordValidator validator : this.validatorList) {
            if(validator.isValid(record) == false) {
                return false;
            }
        }

        return true;
    }
}
