package com.markit.gleif.validator;

import java.util.Map;

/**
 * Created by jc on 12/02/2017.
 */
public interface LEIRecordValidator {

    /**
     * Validates the given record and return true if valid
     *
     * @param record
     * @return
     */
    boolean isValid(Map<String, String> record);
}
